package com.frameworkLib.appium;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Instant;

public class AppiumActions {
    private static final  org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AppiumActions.class.getName());


    public static int defaultWaitTime = 120;

    private static WebElement element(AppiumDriver<?> driver, By by, String locatorName) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, defaultWaitTime);
            wait.until(ExpectedConditions.visibilityOfElementLocated(by));
        } catch (Exception e) {
            logger.debug("Element is not visible : " + locatorName);
        }
        return driver.findElement(by);
    }


    private static WebElement element(AppiumDriver<?> driver,By by, String locatorName,int waitTime) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, waitTime);
            wait.until(ExpectedConditions.visibilityOfElementLocated(by));
        } catch (Exception e) {
            logger.debug("Element is not visible : " + locatorName);
        }
        return driver.findElement(by);
    }
    public static void click(AppiumDriver<?> driver,By by, String locatorName) {
        Instant startTime = Instant.now();
        try {
            element(driver,by,locatorName).click();
            AppiumLog.addReport(true, locatorName, startTime);
        } catch (Exception e) {
            AppiumLog.addReport(false, locatorName, startTime);
            throw new RuntimeException(e);
        }
    }
}

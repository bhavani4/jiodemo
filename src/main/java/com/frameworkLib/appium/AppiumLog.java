package com.frameworkLib.appium;

import com.frameworkLib.reporting.extentReportsSetup.ExtentTestManager;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.TimeZone;

public class AppiumLog {
    private final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AppiumLog.class.getName());

    private static String getCallerMethodName(int methodIndexNum) {
        String methodName = "";
        try {
            StackTraceElement[] stElements = Thread.currentThread().getStackTrace();
            methodName = stElements[methodIndexNum].getMethodName();
        } catch (Exception e) {
            logger.debug("getCallerMethodName failed");
        }
        return methodName;
    }

    private static String getCallerClassName(int classIndexNum) {
        String className = "";
        try {
            StackTraceElement[] stElements = Thread.currentThread().getStackTrace();
            className = stElements[classIndexNum].getClassName();
        } catch (Exception e) {
            logger.debug("getCallerMethodName failed");
        }
        return className;
    }

    private static String getTimeDiff(Instant startTime) {
        Date d = new Date(Duration.between(startTime, Instant.now()).toMillis());
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss:SSS");
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        return df.format(d);
    }

    private static void captureScreenshot(String screenshotMsg) {
        try {

        } catch (Exception e) {
            logger.info("Capture screenshot failed :" + e);
        }
    }

    public static void info(String infoMessage, Instant startTime) {
        String duration = getTimeDiff(startTime);
        logger.info("[INFO] " + infoMessage + "  **** Duration : " + duration);
        try {
            ExtentTestManager.getTest().info(infoMessage + "  **** Duration : " + duration);
        } catch (Exception e) {
            logger.debug("ExtentTestManager get test failed and not captured log for INFO test");
        }
        captureScreenshot("[INFO] " + infoMessage + " || " + duration);
    }

    public static void info(String infoMessage) {
        logger.info("[INFO] " + infoMessage);

        try {
            ExtentTestManager.getTest().info(infoMessage);
        } catch (Exception e) {
            logger.debug("ExtentTestManager get test failed and not captured log for INFO test");
        }
        captureScreenshot("[INFO] " + infoMessage);
    }

    private static void pass(String successMessage, Instant startTime) {
        String duration = getTimeDiff(startTime);
        logger.info("[PASSED] " + successMessage + "  **** Duration : " + duration);
        try {
            ExtentTestManager.getTest().pass(successMessage + "  ***** Duration : " + duration);
        } catch (Exception e) {
            logger.debug("ExtentTestManager get test failed and not captured log for PASSED test");
        }
        captureScreenshot(successMessage + " || " + duration);
    }

    private static void pass(boolean includeInReports, String successMessage, Instant startTime) {
        String duration = getTimeDiff(startTime);
        // logger.info(".." + getCallerClassName(4) + "." + getCallerMethodName(4) + "::  SUCCESS : " + successMessage + "  ********** Duration : " + duration);
        logger.info("[PASSED] " + successMessage + "  **** Duration : " + duration);
        if (includeInReports) {
            try {
                ExtentTestManager.getTest().pass(successMessage + "  ***** Duration : " + duration);
            } catch (Exception e) {
                logger.debug("ExtentTestManager get test failed and not captured log for PASSED test");
            }
            captureScreenshot(successMessage + " || " + duration);
        }
    }

    public static void pass(String successMessage) {
        // logger.info(".." + getCallerClassName(4) + "." + getCallerMethodName(4) + "::  SUCCESS : " + successMessage);
        logger.info("[PASSED] " + successMessage);
        try {
            ExtentTestManager.getTest().pass(successMessage);
        } catch (Exception e) {
            logger.debug("ExtentTestManager get test failed and not captured PASSED log");
        }
        captureScreenshot(successMessage);
    }

    public static void pass(boolean includeInReports, String successMessage) {
        logger.info("[PASSED] " + successMessage);
        if (includeInReports) {
            // logger.info(".." + getCallerClassName(4) + "." + getCallerMethodName(4) + "::  SUCCESS : " + successMessage);
            try {
                ExtentTestManager.getTest().pass(successMessage);
            } catch (Exception e) {
                logger.debug("ExtentTestManager get test failed and not captured PASSED log");
            }
            captureScreenshot(successMessage);
        }
    }


    public static void fail(String message, Throwable rootCause) {
        // logger.error(".." + getCallerClassName(3) + "." + getCallerMethodName(3) + "::  FAILED : " + message, rootCause);
        logger.error("[FAILED] " + message, rootCause);
        try {
            ExtentTestManager.getTest().fail(message);
            ExtentTestManager.getTest().fail(rootCause);
        } catch (Exception e) {
            logger.debug("ExtentTestManager get test failed and not captured FAILED log");
        }
        captureScreenshot("[FAILED] " + message);
    }

    public static void fail(String failedMessage) {
        logger.error("[FAILED] " + failedMessage);
        try {
            ExtentTestManager.getTest().fail(failedMessage);
        } catch (Exception e) {
            logger.debug("ExtentTestManager get test failed and not captured FAILED log");
        }
        captureScreenshot("[FAILED] " + failedMessage);
    }

    public static void fail(String failedMessage, Instant startTime) {
        String duration = getTimeDiff(startTime);
        logger.error("[FAILED] " + failedMessage + "  **** Duration : " + duration);
        try {
            ExtentTestManager.getTest().fail(failedMessage + "  **** Duration : " + duration);
        } catch (Exception e) {
            logger.debug("ExtentTestManager get test failed and not captured log for FAILED test");
        }
        captureScreenshot("[FAILED] " + failedMessage + " || " + duration);
    }

    public static void fail(boolean includeInReports, String failedMessage, Instant startTime) {
        String duration = getTimeDiff(startTime);
        logger.error("[FAILED] " + failedMessage + "  **** Duration : " + duration);
        if (includeInReports) {
            try {
                ExtentTestManager.getTest().fail(failedMessage + "  **** Duration : " + duration);
            } catch (Exception e) {
                logger.debug("ExtentTestManager get test failed and not captured log for FAILED test");
            }
            captureScreenshot("[FAILED] " + failedMessage + " || " + duration);
        }
    }


    public static void debug(String message, String duration) {
        logger.debug("[DEBUG] " + message + "  **** Duration : " + duration);
        try {
            ExtentTestManager.getTest().warning(message + "  **** Duration : " + duration);
        } catch (Exception e) {
            logger.debug("ExtentTestManager get test failed and not captured log for DEBUG test");
        }
        captureScreenshot("[DEBUG] " + message + " || " + duration);

    }

    public static void debug(String message) {
        logger.debug("[DEBUG] " + message);
        try {
            ExtentTestManager.getTest().warning(message);
        } catch (Exception e) {
            logger.debug("ExtentTestManager get test failed and not captured log for DEBUG test");
        }
        captureScreenshot("[DEBUG] " + message);
    }


    /**
     * Implemented Mobile UI Automation - this method is used in all appium action class methods
     *
     * @param reportStatus
     * @param locatorName
     * @param startTime
     */
    public static void addReport(boolean reportStatus, Object locatorName, Instant startTime) {
        if (!reportStatus) {
            fail("[" + getCallerMethodName(3) + " : " + locatorName + "]", startTime);
        } else {
            pass("[" + getCallerMethodName(3) + " : " + locatorName + "]", startTime);
        }
    }

    public static void addReport(boolean includeInReports, boolean reportStatus, Object locatorName, Instant startTime) {

        if (includeInReports) {
            if (!reportStatus) {
                fail("[" + getCallerMethodName(3) + " : " + locatorName + "]", startTime);
            } else {
                pass("[" + getCallerMethodName(3) + " : " + locatorName + "]", startTime);
            }
        }
    }

    /**
     * Implemented in Mobile UI Automation - this method is used in all appium action class methods
     *
     * @param reportStatus    - final action status
     * @param locatorName
     * @param startTime
     * @param moreSuccessInfo
     */
    public static void addReport(boolean reportStatus, Object locatorName, Instant startTime, String moreSuccessInfo) {
        if (!reportStatus) {
            fail("[" + getCallerMethodName(3) + " : " + locatorName + "]", startTime);
        } else {
            pass("[" + getCallerMethodName(3) + " : " + locatorName + "] [More Info :: " + moreSuccessInfo, startTime);
        }
    }

    public static void addReport(boolean includeInReports, boolean reportStatus, Object locatorName, Instant startTime, String moreSuccessInfo) {

        if (includeInReports) {
            if (!reportStatus) {
                fail("[" + getCallerMethodName(3) + " : " + locatorName + "]", startTime);
            } else {
                pass("[" + getCallerMethodName(3) + " : " + locatorName + "] [More Info :: " + moreSuccessInfo, startTime);
            }
        }

    }

    public static void addReport(boolean reportStatus, String message, Instant startTime) {
        if (!reportStatus) {
            fail(message, startTime);
        } else {
            pass(message, startTime);
        }
    }

    public static void addReport(boolean includeInReports, boolean reportStatus, String message, Instant startTime) {
        if (!reportStatus) {
            fail(includeInReports, message, startTime);
        } else {
            pass(includeInReports, message, startTime);
        }
    }

    public static void addReport(boolean reportStatus, String message) {
        if (!reportStatus) {
            fail(message);
        } else {
            pass(message);
        }
    }

    public static void addReport(boolean includeInReports, boolean reportStatus, String message) {
        if (includeInReports) {
            if (!reportStatus) {
                fail(message);
            } else {
                pass(message);
            }
        }
    }
}
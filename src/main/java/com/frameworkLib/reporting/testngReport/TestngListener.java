package com.frameworkLib.reporting.testngReport;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.Duration;
import java.time.Instant;

import com.frameworkLib.Constants;
import com.frameworkLib.reporting.extentReportsSetup.ExtentManager;
import com.frameworkLib.reporting.extentReportsSetup.ExtentTestManager;
import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import com.aventstack.extentreports.Status;

public class TestngListener implements ITestListener {
    final static Logger logger = Logger.getLogger(TestngListener.class.getName());
    Instant start;
    Instant end;
    public void onStart(ITestContext context) {
        System.out.println("*** Test Suite " + context.getName() + " started ***");
    }

    public static void copyAndReplaceFile(String sourcePath, String destPath) {
        try {
            Path source = Paths.get(sourcePath);
            Path dest = Paths.get(destPath);
            Files.copy(source, dest, StandardCopyOption.REPLACE_EXISTING);
            logger.info("File Copied successfully from :" + sourcePath + " to " + destPath);
        } catch (IOException e) {
            logger.error("Failed to copy from :" + sourcePath + " to " + destPath + " exception is :" + e);
        }
    }

    public void onFinish(ITestContext context) {
        System.out.println(("*** Test Suite " + context.getName() + " ending ***"));
        ExtentTestManager.endTest();
        ExtentManager.getInstance().flush();
        copyAndReplaceFile(ExtentManager.reportFileLocation, Constants.REPORTS_FOLDER_DEFAULT + Constants.FILE_SEPARATOR + "ExtentReport.html");
    }


    public void onTestStart(ITestResult result) {
        start = Instant.now();
        String classsName = result.getTestClass().getName();
        System.out.println(("*** Running test method " + result.getMethod().getMethodName() + "..."));
        logger.info("##################### Running Test (Method) : " + classsName+ ".**." + result.getMethod().getMethodName());
        ExtentTestManager.startTest(result.getMethod().getMethodName());
    }

    public void updateTestStatus(String className,ITestResult result, Duration timeElapsed) {
        if (result.getStatus() == 1) {
            ExtentTestManager.getTest().log(Status.PASS, "Test passed");
        } else if (result.getStatus() == 2) {
            ExtentTestManager.getTest().log(Status.FAIL, "Test Failed");
        }else if (result.getStatus() == 3) {
            ExtentTestManager.getTest().log(Status.SKIP, "Test Skipped");
        }else {
            ExtentTestManager.getTest().log(Status.INFO, "Test Executed and result id is : "+ result.getStatus());
        }
    }

    public void onTestSuccess(ITestResult result) {
        end = Instant.now();
        Duration timeElapsed = Duration.between(start, end);
        String classsName = result.getTestClass().getName();
        System.out.println("*** Executed " + result.getMethod().getMethodName() + " test successfully...");
        updateTestStatus(classsName,result,timeElapsed);
        logger.info("##################### Executed Test (Method) : " + classsName+ ".**." + result.getMethod().getMethodName());

    }

    public void onTestFailure(ITestResult result) {
        end = Instant.now();
        Duration timeElapsed = Duration.between(start, end);
        String classsName = result.getTestClass().getName();
        System.out.println("*** Test execution " + result.getMethod().getMethodName() + " failed...");
        ExtentTestManager.getTest().log(Status.FAIL, result.getThrowable());
    }

    public void onTestSkipped(ITestResult result) {
        end = Instant.now();
        Duration timeElapsed = Duration.between(start, end);
        String classsName = result.getTestClass().getName();
        System.out.println("*** Test " + result.getMethod().getMethodName() + " skipped...");
        updateTestStatus(classsName,result,timeElapsed);
    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        System.out.println("*** Test failed but within percentage % " + result.getMethod().getMethodName());
    }

}

package com.frameworkLib.reporting.extentReportsSetup;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.frameworkLib.Constants;
import org.apache.commons.lang3.SystemUtils;

import java.io.File;

public class ExtentManager {

    private static ExtentReports extent;
    private static final String reportFilepath = Constants.SURFIRE_REPORTS_DIR;
    public static String reportFileLocation = Constants.REPORTS_FOLDER + Constants.FILE_SEPARATOR +"ExtentReport.html";

    public static ExtentReports getInstance() {
        if (extent == null)
            createInstance();
        return extent;
    }

    // Create an extent report instance
    public static ExtentReports createInstance() {
        String fileName = getReportPath(reportFilepath);

        ExtentSparkReporter htmlReporter = new ExtentSparkReporter(fileName);
        // htmlReporter.config().setsetTestViewChartLocation(ChartLocation.BOTTOM);
        // htmlReporter.config().setChartVisibilityOnOpen(true);
        htmlReporter.config().setTheme(Theme.DARK);
        htmlReporter.config().setDocumentTitle(Constants.EXTENT_REPORT_FILENAME);
        htmlReporter.config().setEncoding("utf-8");
        htmlReporter.config().setReportName(Constants.EXTENT_REPORT_TITLE);
        htmlReporter.config().setTimeStampFormat("EEEE, MMMM dd, yyyy, hh:mm a '('zzz')'");

        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);
        // Set environment details
        extent.setSystemInfo("Suit Name",  "DEMO SUITE");
        extent.setSystemInfo("Author Name", "Bhavani K");
        extent.setSystemInfo("OS", SystemUtils.OS_NAME);
        extent.setSystemInfo("OS Version", SystemUtils.OS_VERSION);
        return extent;
    }

    // Create the report path
    private static String getReportPath(String path) {
        File testDirectory = new File(path);
        if (!testDirectory.exists()) {
            if (testDirectory.mkdir()) {
                System.out.println("Directory: " + path + " is created!");
                return reportFileLocation;
            } else {
                System.out.println("Failed to create directory: " + path);
                return System.getProperty("user.dir");
            }
        } else {
            System.out.println("Directory already exists: " + path);
        }
        return reportFileLocation;
    }

}

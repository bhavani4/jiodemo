package com.frameworkLib;

import com.frameworkLib.utils.DateAndTimeLib;

import java.io.File;

public class Constants {

    public final static String DIR = System.getProperty("user.dir");
    public final static String FILE_SEPARATOR = System.getProperty("file.separator");
    public final static String UI_REPORTS_PATH = "./Reports/UI_Reports/" + DateAndTimeLib.getCurrentDate();

    public final static String SURFIRE_REPORTS_DIR = DIR + "/target/surefire-reports";
    public final static File REPORTS_FOLDER = new File(UI_REPORTS_PATH);

    public final static String EXTENT_REPORT_FILENAME = "ExtentAutomatonReport.html";
    public final static String EXTENT_REPORT_TITLE = "QA Automation";
    public final static File REPORTS_FOLDER_DEFAULT = new File("./Reports/");


}

package com.frameworkLib.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateAndTimeLib {


    public static String getCurrentDate() {
        Date currentDate = new Date();
        SimpleDateFormat newFormat = new SimpleDateFormat("ddMMyyyy-HHmmssSSSS");
        return newFormat.format(currentDate);
    }
}
